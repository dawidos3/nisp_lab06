#include "student-c.h"
#include "math.h"

int isPrime(int digitA) {

    for (int i = 2; i <= sqrt(digitA); i++) {
        if (digitA % i == 0) {
          return "not prime";
        }
      }
      return "prime";
}

int average(int [ ] digits, int size){

       int total=0,i;
       for(int i=0; i<size; i++){
           total= total+ digits[i];
        }
       double avg = total/size;
   return avg;
}
