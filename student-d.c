#include "student-d.h"

int NWD(int a, int b){
    do
    {
        if(a > b) {
            a = a - b;
        } else {
            b = b - a;
        }
    }
    while(a != b);
    return a;
}

int NWW(int a, int b)
{
	int pom;

  	while(b!=0)
	{
    	pom = b;
    	b = a%b;
    	a = pom;  
  	}
    return a;
}
