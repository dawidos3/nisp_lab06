cmake_minimum_required(VERSION 3.5)

project(nisp_lab6 LANGUAGES C)

add_executable(nisp_lab6 main.c)
